<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>GR InfoTech :: <?php echo $pagename; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="css/custom_style.css" rel="stylesheet">
    <link rel="stylesheet" media="screen" href="css/magicsuggest.css">

    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/magicsuggest.js"></script>

    <script type="text/javascript">
        $( document ).ready(function() {

		/* Footer */
	    	positionFooter(); 
		function positionFooter(){
			var padding_top = $("#footer").css("padding-top").replace("px", "");
			var page_height = $(document.body).height() - padding_top;
			var window_height = $(window).height();
			var difference = window_height - page_height;
			if (difference < 0) 
				difference = 0;

			$("#footer").css({
				padding: difference + "px 0 0 0"
			})
		}

		$(window).resize(positionFooter);

		/* Compose */	
		function deselect(e) {
		  $('.pop').slideFadeToggle(function() {
		    e.removeClass('selected');
		  });    
		}

		  $('#compose').on('click', function() {
		    if($(this).hasClass('selected')) {
		      deselect($(this));               
		    } else {
		      $(this).addClass('selected');
		      $('.pop').slideFadeToggle();
		    }
		    return false;
		  });

		  $('.close1').on('click', function() {
		    deselect($('#compose'));
		    return false;
		  });

	

		$.fn.slideFadeToggle = function(easing, callback) {
		  return this.animate({ opacity: 'toggle', height: 'toggle' }, 'fast', easing, callback);
		};


        }); // end of jquery
    </script>
<style type="text/css">
.ms-trigger{
	display:none;
}
</style>
</head>
<body>
<div class="navbar-wrapper">
<div class="container">
    <div class="navbar navbar-inverse navbar-static-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span><span
                        class="icon-bar"></span><span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">GR MSG</a>
            </div>
            <div class="navbar-collapse collapse">
               <!-- <ul class="nav navbar-nav">
                    <li><a href="#">Home</a></li>
                </ul> -->
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo get_session("LOGIN_EMAILID"); ?>
                        <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="navbar-content">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <img src="images/photo.jpg"
                                                alt="Alternate Text" class="img-responsive" />
                                            <p class="text-center small">
                                                <a href="#">Change Photo</a></p>
                                        </div>
                                        <div class="col-md-7">
                                            <span><?php echo get_session("DISPLAY_UNAME"); ?></span>
                                            <p class="text-muted small">
                                                <?php echo get_session("LOGIN_EMAILID"); ?></p>
                                            <div class="divider">
                                            </div>
                                            <a href="#" class="btn btn-primary btn-sm active">View Profile</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="navbar-footer">
                                    <div class="navbar-footer-content">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <a href="#" class="btn btn-default btn-sm">Change Passowrd</a>
                                            </div>
                                            <div class="col-md-6">
                                                <a href="#" class="btn btn-default btn-sm pull-right">Sign Out</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
</div>

<br><br><br><br>

