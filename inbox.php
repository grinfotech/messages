<?php
session_start();
include('dbcon.php');
include('genfunctions.php');

$pagename="Inbox";
$pageno="1";

include('msg_header.php');
?>
<div class="container">
    <?php include('msg_top_row.php'); ?>
    <div class="row">
        <div class="col-sm-3 col-md-2">
	<?php include('msg_left_nav.php'); ?>
        </div>
        <div class="col-sm-9 col-md-10">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs">
                <li class="active"><a href="#home" data-toggle="tab"><span class="glyphicon glyphicon-inbox">
                </span>Primary</a></li>
                <li><a href="#profile" data-toggle="tab"><span class="glyphicon glyphicon-user"></span>
                    Social</a></li>
                <li><a href="#messages" data-toggle="tab"><span class="glyphicon glyphicon-tags"></span>
                    Promotions</a></li>
                <li><a href="#settings" data-toggle="tab"><span class="glyphicon glyphicon-plus no-margin">
                </span></a></li>
            </ul>
            <!-- Tab panes -->
<?php

$cur_userid=get_session('LOGIN_UID');

$inbox_sql=mysql_query("SELECT * from inbox where msguserid='$cur_userid' and find_in_set('$cur_userid',to_userids) and status='0' GROUP BY msg_seqid order by createddate desc")or die("INBOX : ".mysql_error());

$inbox_reply_sql=mysql_query("select * from inbox_reply where msguserid='$cur_userid' and find_in_set('$cur_userid',to_userids) and status='0' GROUP BY msg_seqid, reply_seqid order by createddate desc")or die("INBOX REPLY: ".mysql_error());

$inbox=array();

$k=0; // inbox array indexing

$i=0;
while($ibr=mysql_fetch_array($inbox_sql))
{
	$cur_msgid=$ibr['id'];
	$cur_msgseqid=$ibr['msg_seqid'];
	
$reply_sql=mysql_query("SELECT * from inbox_reply WHERE status='0' and msg_seqid='$cur_msgseqid' group by msg_seqid, reply_seqid, from_userid") or die("Inbox Reply : ".mysql_error());
$ibcnt=mysql_num_rows($reply_sql);
	
	$inbox[$k]['id']=$ibr['id'];
	$inbox[$k]['msgseqid']=$ibr['msg_seqid'];
	$inbox[$k]['replyseqid']=0;
	$inbox[$k]['inboxid']="0";
	$inbox[$k]['from_userid']=$ibr['from_userid'];
	$inbox[$k]['from_username']=$ibr['from_username'];
	$inbox[$k]['subject']=$ibr['subject'];
	$inbox[$k]['read_flag']=$ibr['read_flag'];
	$inbox[$k]['star_flag']=$ibr['star_flag'];
	$inbox[$k]['createddate']=$ibr['createddate'];
	$inbox[$k]['replycnt']=$ibcnt;
	$inbox[$k]['msgtype']="inbox";
	$inbox[$k]['attachment_ids']=$ibr['attachment_ids'];
	$inbox[$k]['body']=$ibr['body'];

	$i++;
	
	$k++;
}

// for Reply Mails

$i=0;
while($ibr=mysql_fetch_array($inbox_reply_sql))
{
	$cur_msgid=$ibr['id'];
	$cur_msgseqid=$ibr['msg_seqid'];
	
$replycnt_sql=mysql_query("SELECT * FROM inbox_reply WHERE status = '0' AND msg_seqid = '$cur_msgseqid' group by msg_seqid, reply_seqid, from_userid") or die("Reply : ".mysql_error());
$ibrcnt=mysql_num_rows($replycnt_sql);

	$inbox[$k]['id']=$ibr['id'];
	$inbox[$k]['msgseqid']=$ibr['msg_seqid'];
	$inbox[$k]['replyseqid']=$ibr['reply_seqid'];
	$inbox[$k]['inboxid']=$ibr['inbox_id'];
	$inbox[$k]['from_userid']=$ibr['from_userid'];
	$inbox[$k]['from_username']=$ibr['from_username'];
	$inbox[$k]['subject']=$ibr['subject'];
	$inbox[$k]['read_flag']=$ibr['read_flag'];
	$inbox[$k]['star_flag']=$ibr['star_flag'];
	$inbox[$k]['createddate']=$ibr['createddate'];
	$inbox[$k]['replycnt']=$ibrcnt;
	$inbox[$k]['msgtype']="reply";
	$inbox[$k]['attachment_ids']=$ibr['attachment_ids'];
	$inbox[$k]['body']=$ibr['body'];
	
	$i++;

	$k++;
}

$inbox = array_map("unserialize", array_unique(array_map("serialize", $inbox)));


function array_sort_by_column(&$array, $column, $direction = SORT_DESC) {
    $reference_array = array();

    foreach($array as $key => $row) {
        $reference_array[$key] = $row[$column];
    }

    array_multisort($reference_array, $direction, $array);
}

array_sort_by_column($inbox, 'createddate');
 
$maxmsgseqid=0;
$maxreplyseqid=0;

$inboxcnt=0;

?>
            <div class="tab-content">
                <div class="tab-pane fade in active" id="home">

                    <div class="list-group">

<?php
if(count($inbox)!=0)
{

for($i=0;$i<count($inbox);$i++)
{
	$cbclass="";

	if($inbox[$i]['read_flag']=='0')
	{ 
		echo "<div class='list-group-item' style='cursor:pointer;'>";	
		$cbclass.=" unreadmsg";
	}else{
		echo "<div class='list-group-item read' style='cursor:pointer;'> ";
		$cbclass.=" readmsg";
	}

	if($inbox[$i]['star_flag']=="0")
	{	
		$cbclass.=" unstarmsg";
	}else if($inbox[$i]['star_flag']=="1")
	{
		$cbclass.=" starmsg";
	}

?>
            <div class="checkbox">
                <label>
		<?php if($inbox[$i]['msgtype']=="reply"){ ?>
	<input type="checkbox" name="ibck" style="cursor:pointer;" class="<?php echo $cbclass; ?>" value="<?php echo $inbox[$i]['msgseqid'].'_'.$inbox[$i]['inboxid'].'_'.$inbox[$i]['id']; ?>"/>
		<?php   }else if($inbox[$i]['msgtype']=="inbox"){ ?>
	<input type="checkbox" name="ibck" style="cursor:pointer;" class="<?php echo $cbclass; ?>" value="<?php echo $inbox[$i]['msgseqid'].'_'.$inbox[$i]['id'].'_0'; ?>" />
		<?php	} ?>
                </label>
            </div>
<?php
if($inbox[$i]['star_flag']=="0")
{?>
<span class="glyphicon glyphicon-star-empty" onclick="javascript:starfun(0,<?php echo $inbox[$i]['id']; ?>,'<?php echo $inbox[$i]['msgtype']; ?>');"></span>
<?php
}else if($inbox[$i]['star_flag']=="1"){
?>
<span class="glyphicon glyphicon-star" onclick="javascript:starfun(1,<?php echo $inbox[$i]['id']; ?>,'<?php echo $inbox[$i]['msgtype']; ?>');"></span>
<?php
}
?>

	    <span class="name" style="min-width: 120px;display: inline-block;" title="<?php echo ucfirst($inbox[$i]['from_username']); ?>">
<?php 

if($inbox[$i]['read_flag']=='0')
{
	if($inbox[$i]['msgtype']=="reply")
	{ 
	echo "<a href='viewmsg.php?msgseqid=".$inbox[$i]['msgseqid']."&msgid=".$inbox[$i]['inboxid']."&replymsgid=".$inbox[$i]['id']."' >".ucfirst($inbox[$i]['from_username'])."</a>";
	}else if($inbox[$i]['msgtype']=="inbox"){
	echo "<a href='viewmsg.php?msgseqid=".$inbox[$i]['msgseqid']."&msgid=".$inbox[$i]['id']."'>".ucfirst($inbox[$i]['from_username'])."</a>";
	}

}else{
	if($inbox[$i]['msgtype']=="reply")
	{ 
	echo "<a href='viewmsg.php?msgseqid=".$inbox[$i]['msgseqid']."&msgid=".$inbox[$i]['inboxid']."&replymsgid=".$inbox[$i]['id']."'  style='color:#555;' >".ucfirst($inbox[$i]['from_username'])."</a>";
	}else if($inbox[$i]['msgtype']=="inbox"){
	echo "<a href='viewmsg.php?msgseqid=".$inbox[$i]['msgseqid']."&msgid=".$inbox[$i]['id']."' style='color:#555;'>".ucfirst($inbox[$i]['from_username'])."</a>";
	}
}
		if($inbox[$i]['replycnt']>0)
		{				
			echo ' <span class="grey">('.$inbox[$i]['replycnt'].')</span>';
		}
	    ?>
	</span> 
	    <span class="">
<?php

if($inbox[$i]['read_flag']=='0')
{
	if($inbox[$i]['msgtype']=="reply")
	{
	echo "<a href='viewmsg.php?msgseqid=".$inbox[$i]['msgseqid']."&msgid=".$inbox[$i]['inboxid']."&replymsgid=".$inbox[$i]['id']."' >".ucfirst($inbox[$i]['subject'])."</a>";
	}else if($inbox[$i]['msgtype']=="inbox"){
	echo "<a href='viewmsg.php?msgseqid=".$inbox[$i]['msgseqid']."&msgid=".$inbox[$i]['id']."'>".ucfirst($inbox[$i]['subject'])."</a>";
	}
}else{
	if($inbox[$i]['msgtype']=="reply")
	{
	echo "<a href='viewmsg.php?msgseqid=".$inbox[$i]['msgseqid']."&msgid=".$inbox[$i]['inboxid']."&replymsgid=".$inbox[$i]['id']."'  style='color:#555;' >".ucfirst($inbox[$i]['subject'])."</a>";
	}else if($inbox[$i]['msgtype']=="inbox"){
	echo "<a href='viewmsg.php?msgseqid=".$inbox[$i]['msgseqid']."&msgid=".$inbox[$i]['id']."' style='color:#555;'>".ucfirst($inbox[$i]['subject'])."</a>";
	}
}

?>
	    </span>
            <span class="text-muted" style="font-size: 11px;">- <?php echo limit_words(strip_tags($inbox[$i]['body']),5); ?></span> 
	    <span class="badge">
	    <?php 
			$createddate=strtotime($inbox[$i]['createddate']); 
			echo date('g:i a',$createddate); 
	    ?></span> 
	   <span class="pull-right">
	   <?php
		if($inbox[$i]['attachment_ids']!="")
		{
	   ?>
		<span class="glyphicon glyphicon-paperclip"></span>
	   <?php 
		}
	   ?>
	  </span>
	</div>
<?php

} // end of for loop

}else{
?>	<br>
  <span class="text-center">Your Inbox is empty.</span>
<?php
}// end of if count 
?>
		<!--	<a href="#" class="list-group-item">
		            <div class="checkbox">
		                <label>
		                    <input type="checkbox">
		                </label>
		            </div>
                            <span class="glyphicon glyphicon-star-empty"></span>
			    <span class="name" style="min-width: 120px; display: inline-block;">Bhaumik Patel</span> 
			    <span class="">This is big title</span>
                            <span class="text-muted" style="font-size: 11px;">- Hi hello how r u ?</span> 
			    <span class="badge">12:10 AM</span> <span class="pull-right"><span class="glyphicon glyphicon-paperclip"></span></span>
			</a>

			<a href="#" class="list-group-item read">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" >
                                </label>
                            </div>
                            <span class="glyphicon glyphicon-star"></span><span class="name" style="min-width: 120px;
                                display: inline-block;">Bhaumik Patel</span> <span class="">This is big title</span>
                            <span class="text-muted" style="font-size: 11px;">- Hi hello how r u ?</span> <span
                                class="badge">12:10 AM</span> <span class="pull-right"><span class="glyphicon glyphicon-paperclip">
                                </span></span>
			</a>-->


                    </div> <!-- end of class list-group-->

                </div>
                <div class="tab-pane fade in" id="profile">
                    <div class="list-group">
                        <div class="list-group-item">
                            <span class="text-center">This tab is empty.</span>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade in" id="messages">
                    ...</div>
                <div class="tab-pane fade in" id="settings">
                    This tab is empty.</div>
            </div>
<script type="text/javascript">

function checkboxs(type)
{
	//alert(type);

	if(type=="all")
	{
		$('input:checkbox[name="ibck"]').attr("checked", true);
	}
	else if(type=="none")
	{
		$('input:checkbox[name="ibck"]').attr("checked", false);
	}
	else if(type=="read")
	{
		$('.starmsg').attr("checked", false);
		$('.unstarmsg').attr("checked", false);
		$('.unreadmsg').attr("checked", false);

		$('.readmsg').attr("checked", true);	
		
	}
	else if(type=="unread")
	{
		$('.readmsg').attr("checked", false);
		$('.starmsg').attr("checked", false);
		$('.unstarmsg').attr("checked", false);

		$('.unreadmsg').attr("checked", true);
		
	}
	else if(type=="star")
	{
		$('.readmsg').attr("checked", false);
		$('.unreadmsg').attr("checked", false);
		$('.unstarmsg').attr("checked", false);

		$('.starmsg').attr("checked", true);
		
	}
	else if(type=="unstar")
	{
		$('.readmsg').attr("checked", false);
		$('.unreadmsg').attr("checked", false);
		$('.starmsg').attr("checked", false);

		$('.unstarmsg').attr("checked", true);
		
		
	}
}

function starfun(curstar,msgid,msgtype)
{
	
	var data = {
			type: 'starmsg',
			curstar: curstar,
			msgid: msgid,
			msgtype: msgtype,
			tablename: 'inbox'
		 }

	$.ajax({
			type: "POST",
			url: "message_actions.php",
			data: data,
			success: function(resp) {
				window.location.href="inbox.php";
		    	},
		    	error: function() {
				alert('Error while Staring');
		    	},
	});

}

function readcheckedfun(readstatus)
{
	
	var checkBoxes = document.getElementsByTagName('input');
       
        var param = "";
        for (var counter=0; counter < checkBoxes.length; counter++) {
                        if (checkBoxes[counter].type.toUpperCase()=='CHECKBOX' && checkBoxes[counter].checked == true) {
                                        param += checkBoxes[counter].value+",";
                        }
        }	

	var data = {
			type: 'inboxreadmsg',
			curstatus: readstatus,
			msgseqid: param
		 }

	$.ajax({
			type: "POST",
			url: "message_actions.php",
			data: data,
			success: function(resp) {
				window.location.href="inbox.php";
		    	},
		    	error: function() {
				alert('Error while Read/Unread Update!');
		    	},
	});

}

function starcheckedfun(starstatus)
{
	
	var checkBoxes = document.getElementsByTagName('input');
       
        var param = "";
        for (var counter=0; counter < checkBoxes.length; counter++) {
                        if (checkBoxes[counter].type.toUpperCase()=='CHECKBOX' && checkBoxes[counter].checked == true) {
                                        param += checkBoxes[counter].value+",";
                        }
        }	

	var data = {
			type: 'inboxstarmsg',
			curstatus: starstatus,
			msgseqid: param
		 }

	$.ajax({
			type: "POST",
			url: "message_actions.php",
			data: data,
			success: function(resp) {
				window.location.href="inbox.php";
		    	},
		    	error: function() {
				alert('Error while Star/Unstar Update!');
		    	},
	});

}

function deletefun(status)
{
	
	var checkBoxes = document.getElementsByTagName('input');
       
        var param = "";
        for (var counter=0; counter < checkBoxes.length; counter++) {
                        if (checkBoxes[counter].type.toUpperCase()=='CHECKBOX' && checkBoxes[counter].checked == true) {
                                        param += checkBoxes[counter].value+",";
                        }
        }	

	var data = {
			type: 'inboxdeletemsg',
			curstatus: status,
			msgseqid: param
		 }

	$.ajax({
			type: "POST",
			url: "message_actions.php",
			data: data,
			success: function(resp) {
				window.location.href="inbox.php";
		    	},
		    	error: function() {
				alert('Error while Msg Status Update!');
		    	},
	});

}

$(document).ready(function(){



});
</script>
<?php 
include('msg_footer.php');
?>
