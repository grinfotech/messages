<?php
session_start();
include('dbcon.php');
include('genfunctions.php');

$pagename="View Messages";
$pageno="2";

$replyto="0";

include('msg_header.php');
?>
<style>

.panel-group .panel {
	border-radius: 0px 0px 0px 0px;
	margin-bottom: -6px;
	overflow: visible;
}

.panel-title{
	font-size:14px;
}

.msg_subject{
	font-size:16px;
}

.time{
	font-size:12px;
}

.contleft{
	float:left;
	width:75%;
	text-align:left;
	line-height:16px;
}

.contright{
	float:right;
	width:25%;
	line-height:17px;
}

.panel-heading{
	height: 52px;
	padding: 7px;
	line-height:25px;
}

.panel {
    border-radius: 0px 0px 0px 0px;
}
</style>
<div class="container">
    <?php include('msg_top_row.php'); ?>
    <div class="row">
        <div class="col-sm-3 col-md-2">
	<?php include('msg_left_nav.php'); ?>
        </div>
        <div class="col-sm-9 col-md-10">
<?php
$cur_userid=get_session('LOGIN_UID');

$ibtolist=array();
$ibtolistids=array();
$cur_replyid="";

if(isset($_REQUEST['msgid']))
{
	$msgid=$_REQUEST['msgid'];
	$msgseqid=$_REQUEST['msgseqid'];

	if(isset($_REQUEST['replymsgid']))
	{
		$cur_replyid=$_REQUEST['replymsgid'];
	}else{
		$cur_replyid=0;
	}

	$upd_as_readmsg_sql=mysql_query("update inbox set read_flag='1' where msg_seqid='$msgseqid' and msguserid='$cur_userid' and status='0'")or die("VIEW MSG : ".mysql_error());

	$upd_as_readmsg_reply_sql=mysql_query("update inbox_reply set read_flag='1' where msg_seqid='$msgseqid' and inbox_id='$msgid' and msguserid='$cur_userid' and status='0'")or die("VIEW MSG : ".mysql_error());	

	$inbox_sql=mysql_query("select * from inbox where msg_seqid='$msgseqid' and msguserid='$cur_userid'")or die("INBOX : ".mysql_error());

	
$reply_inbox_sql=mysql_query("select * from inbox_reply where msg_seqid='$msgseqid' and msguserid='$cur_userid' and status='0' group by msg_seqid, reply_seqid, from_userid order by id asc")or die("INBOX : ".mysql_error());

	$totalreplycnt=mysql_num_rows($reply_inbox_sql);

	$ibr=mysql_fetch_array($inbox_sql);

?>

<div class="msg_subject"> <?php echo $ibr['subject'];  $msgsubject=$ibr['subject']; ?></div>
<div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
<?php
	$fromuserid=$ibr['from_userid'];
	$userimg="";

	$imgr=mysql_query("select imgpath from users where id='$fromuserid'")or die(mysql_error());
	$imgres=mysql_fetch_array($imgr);

	$userimg="images/".$imgres['imgpath'];

	if($userimg=="")
	{
		$userimg="images/photo.jpg";
	}
?>	
	<span class="contleft">
	<img alt="User Image" src="<?php echo $userimg; ?>" width="32" align="left" style="margin-right:5px;"/>
	<span><?php echo ucfirst($ibr['from_username']); ?></span><br>
	<span style="font-size:11px;color:gray;">To : <?php echo $ibr['to_usernames']; ?></span>
	</span>

	<span class="contright">
	<span class="time" style="float:right;">
        <?php 
		$createddate=strtotime($ibr['createddate']); 
		echo date('F j, Y g:i A',$createddate);  
		echo " &nbsp; (".display_time_diff_format($createddate).") "; 
	?>
	</span>
	


	</span> <!-- end of contright class span -->
	<span style="clear:both;"></span>

      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse <?php if($totalreplycnt==0) echo 'in'; ?>">
      <div class="panel-body">

<span>
	<div class="btn-group" style="float:right;">
        <button type="button" class="btn btn-xs btn-default  dropdown-toggle" data-toggle="dropdown">
            <div style="margin: 0;">
                <span class="glyphicon glyphicon-cog"></span>
            </div>
        </button>
        <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
             <span class="caret"></span><span class="sr-only">Toggle Dropdown</span>
        </button>
        <ul class="dropdown-menu pull-right" role="menu">
            <li><a href="viewmsg.php?msgseqid=<?php echo $msgseqid; ?>&msgid=<?php echo $msgid; ?>&action=reply">Reply</a></li>
            <li><a href="viewmsg.php?msgseqid=<?php echo $msgseqid; ?>&msgid=<?php echo $msgid; ?>&action=replyall">Reply to all</a></li>
	    <li class="divider"></li>
            <li>
		<?php
		if($cur_replyid!='')
			$rlyid=$cur_replyid;
		else
			$rlyid=0;
		?>
	    <a href="#" onclick="javascript:readfun(1,<?php echo $msgseqid; ?>,<?php echo $msgid; ?>,<?php echo $rlyid; ?>);">Mark as read</a></li>
            <li><a href="#" onclick="javascript:readfun(0,<?php echo $msgseqid; ?>,<?php echo $msgid; ?>,<?php echo $rlyid; ?>);">Mark unread</a></li>
            <li class="divider"></li>
	    <li><a href="#" onclick="javascript:starfun(1,<?php echo $msgseqid; ?>,<?php echo $msgid; ?>,<?php echo $rlyid; ?>);">Add Star</a></li>
 	    <li><a href="#" onclick="javascript:starfun(0,<?php echo $msgseqid; ?>,<?php echo $msgid; ?>,<?php echo $rlyid; ?>);">Remove Star</a></li>
 	    <li class="divider"></li>
	    <li><a href="#" onclick="javascript:deletefun(0,<?php echo $msgseqid; ?>,<?php echo $rlyid; ?>);">Delete</a></li>
        </ul>
    </div>
</span>
        <?php echo html_entity_decode($ibr['body']); ?>
	
      </div>
    </div>
  </div>

<?php
$k=1;
while($r_ibr=mysql_fetch_array($reply_inbox_sql))
{
	$replyseqid=$r_ibr['reply_seqid'];

	$fromuserid=$r_ibr['from_userid'];

	$userimg="";

	$imgr=mysql_query("select imgpath from users where id='$fromuserid'")or die(mysql_error());
	$imgres=mysql_fetch_array($imgr);

	$userimg="images/".$imgres['imgpath'];

	if($userimg=="")
	{
		$userimg="images/photo.jpg";
	}

	//echo "K : ".$k." ; R : ".$totalreplycnt;

?>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $k; ?>">

       <span class="contleft">
	<img alt="User Image" src="<?php echo $userimg; ?>" width="32" align="left" style="margin-right:5px;"/>
	<span><?php echo $r_ibr['from_username']; ?></span><br>
	<span style="font-size:11px;color:gray;">To : <?php echo $r_ibr['to_usernames']; ?></span>
	</span>

	<span class="contright">
	<span class="time" style="float:right;">
        <?php 
		$createddate=strtotime($r_ibr['createddate']); 
		echo date('F j, Y g:i A',$createddate);  
		echo " &nbsp; (".display_time_diff_format($createddate).") "; 
	?>
	</span>	

	</span> <!-- end of contright class span -->
	<span style="clear:both;"></span>

      </h4>
    </div>
    <div id="collapse<?php echo $k; ?>" class="panel-collapse collapse <?php if($k==$totalreplycnt) echo 'in'; ?>">
      <div class="panel-body">

<span>
	<div class="btn-group" style="float:right;">
        <button type="button" class="btn btn-xs btn-default  dropdown-toggle" data-toggle="dropdown">
            <div style="margin: 0;">
                <span class="glyphicon glyphicon-cog"></span>
            </div>
        </button>
        <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
             <span class="caret"></span><span class="sr-only">Toggle Dropdown</span>
        </button>
        <ul class="dropdown-menu pull-right" role="button">
            <li><a href="viewmsg.php?msgseqid=<?php echo $msgseqid; ?>&msgid=<?php echo $msgid; ?>&action=reply">Reply</a></li>
            <li><a href="viewmsg.php?msgseqid=<?php echo $msgseqid; ?>&msgid=<?php echo $msgid; ?>&action=replyall">Reply to all</a></li>
	    <li class="divider"></li>
            <li>
		<?php
		if($cur_replyid!='')
			$rlyid=$cur_replyid;
		else
			$rlyid=0;
		?>
	    <a href="#" onclick="javascript:readfun(1,<?php echo $msgseqid; ?>,<?php echo $msgid; ?>,<?php echo $rlyid; ?>);">Mark as read</a></li>
            <li><a href="#" onclick="javascript:readfun(0,<?php echo $msgseqid; ?>,<?php echo $msgid; ?>,<?php echo $rlyid; ?>);">Mark unread</a></li>
            <li class="divider"></li>
	    <li><a href="#" onclick="javascript:starfun(1,<?php echo $msgseqid; ?>,<?php echo $msgid; ?>,<?php echo $rlyid; ?>);">Add Star</a></li>
 	    <li><a href="#" onclick="javascript:starfun(0,<?php echo $msgseqid; ?>,<?php echo $msgid; ?>,<?php echo $rlyid; ?>);">Remove Star</a></li>
 	    <li class="divider"></li>
	    <li><a href="#" onclick="javascript:deletefun(0,<?php echo $msgseqid; ?>,<?php echo $rlyid; ?>);">Delete</a></li>
        </ul>
    </div>
</span>

      <?php 
		echo html_entity_decode($r_ibr['body']);			
      ?>
      </div>
    </div>
  </div>


<?php
	$k++;
} // end of reply while
?>

</div> <!-- end of accordion -->

<br>
<?php

}

if(isset($_REQUEST['action']))
{
	if($_REQUEST['action']=="reply")
	{
		$replyto=get_replyto_users2($ibr['from_userid']);
		$replytoid=$ibr['from_userid'];
	}
	else if($_REQUEST['action']=="replyall")
	{
		$replyto_all_list=array();
		$replyto_all_list[0]=$ibr['from_userid'];
		
		$uids=explode(",",$ibr['to_userids']);
		
		$j=1;
		for($i=0;$i<count($uids);$i++)
		{
			if($uids[$i]!=$cur_userid)
			{
				$replyto_all_list[$j]=$uids[$i];
			}
			$j++;
		}
		$usrlist=implode(",",$replyto_all_list);
		$replyto=get_replyto_users2($usrlist);
		$replytoid=$usrlist;
	}
	
?>
<form method="post">
<div class="panel panel-default">
  <div class="panel-body">
	<input type="text" name="replyto" id="replyto" /> <br>
	<input type="hidden" name="rsubject" id="rsubject" class="form-control" value="<?php echo $msgsubject; ?>" />
	<input type="hidden" name="msg_seqid" id="msg_seqid" value="<?php echo $msgseqid; ?>" />
	<input type="hidden" name="orgmsgid" id="orgmsgid" value="<?php echo $msgid; ?>" />
	<textarea rows="6" name="rmessage" id="rmessage" cols="40" class="form-control"></textarea>
  </div>
  <div class="panel-footer">
	<input type="button" id="btnreply" value="  Send  " name="reply" class="btn btn-primary" /> &nbsp;&nbsp; 
	<a class="btn btn-default" href="#">Cancel</a>
  </div>
</div>
</form>
<?php
}
else{
$replyto="0";
}

?>

<script type="text/javascript">
var repto="";
var reptoid="";

repto=[<?php echo $replyto; ?>];
//reptoid=[<?php echo $replytoid; ?>];

$(document).ready(function(){


$('#btnreply').click(function(){

	var recpt=$('#replyto').val();
	var subject=$('#rsubject').val();
	var message=$('#rmessage').html();
	var orgmsgid=$('#orgmsgid').val();
	//var attids=$('#upfileids').val();
	var msg_seqid=$('#msg_seqid').val();
	
	if(recpt!=null)
	{
		var data = {
				type: 'replytomail',
				sendto: recpt,
				subject: subject,
				message:message,
				orgmsgid:orgmsgid,
				//attids:attids,
				msg_seqid:msg_seqid
			 }

			$.ajax({
				type: "POST",
				url: "message_actions.php",
				data: data,
				success: function(resp) {
					alert("Mail Sent Successfully");
					window.location.href="inbox.php";
			    	},
			    	error: function() {
					alert('Error while Saving');
			    	},
			});
	}else{
		alert("Please Specify atleast one Recipient ");
	}

});


	$('#replyto').magicSuggest({
		placeholder: '',
		data: 'getusers.php',
		name: "replyto",
		resultAsStringDelimiter: ',',
		resultAsString: true,
		renderer: function(data){
		    return '<div style="padding: 5px; overflow:hidden;">' +
		        '<div style="float: left; width:10%;"><img width="100%" src="' + data.picture + '" /></div>' +
		        '<div style="float: left; margin-left: 5px">' +
		            '<div style="font-weight: bold; color: #333; font-size: 13px; line-height: 11px">' + data.name + '</div>' +
		            '<div style="color: #999; font-size: 11px">' + data.email + '</div>' +
		        '</div>' +
		    '</div><div style="clear:both;"></div>'; // make sure we have closed our dom stuff
		},
        }).setSelection(repto);

	//$('#replyto').magicSuggest().setSelection(repto);

	//ms.setSelection([{name:'GR User2', value:3},{name:'GR User1', value:2}]);
	if(repto!=0)
	{
		//ms.setSelection(repto);
		//ms.setValue(reptoid);
	}

	/*$(ms).on('blur', function(c){
	  alert('where are you going?');
		var replytousers=JSON.stringify(ms.getValue());

		$('#replyto').val(replytousers);
	});*/




}); // end of jquery


</script>
<?php 

include('msg_footer.php');
?>
