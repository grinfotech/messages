<?php

$GLOBALS['imgpath'] = 'assets/images/users/';

function get_session($var)
{
	if (isset($_SESSION[$var])) {
		return $_SESSION[$var];
	}
	else {
		return '';
	}
}

function set_session($var, $value)
{
	$_SESSION[$var] = $value;
}

function unset_session($var)
{
	if(isset($_SESSION[$var])) {
		unset($_SESSION[$var]);
	}

}

function validate_login()
{
	
	if( get_session('LOGGED_IN') == '' || get_session('LOGGED_IN') == '0' )
	{
		header('Location: index.php');
		exit();
	}
	
	return;
}

function safe_sql_nq( $val )
{
	  $val = addslashes( htmlentities( $val, ENT_QUOTES, 'UTF-8') );
	  return $val;
}

function safe_sql( $val )
{
	$val = addslasher(htmlentities( $val.ENT_QUOTES, 'UTF-8') );
	return $val;
}

function request_get($param, $type = 1, $default_value = "") {
	if($type == 1){
		if(isset($_REQUEST[$param]) && trim($_REQUEST[$param]) != "") 
			return htmlentities(addslashes(trim($_REQUEST[$param])));
	}
	if($type == 2){
		return $_REQUEST[$param];
	}
	if($default_value !== "")
		return $default_value;
	
	return "";
}

function display_time_diff_format($var_date,$span_flag=0)
{
	$str_datetime = "";
	if ($var_date > 100) {
		$dateDiff = time() - $var_date;
		$fullDays = floor($dateDiff/(60*60*24));
		$fullHours = floor(($dateDiff-($fullDays*60*60*24))/(60*60));
		$fullMinutes = floor(($dateDiff-($fullDays*60*60*24)-($fullHours*60*60))/60);
		$fullSeconds = $dateDiff%60;
		
		if($fullDays == 0 && $fullHours == 0 && $fullMinutes == 0) {
			$str_datetime = $fullSeconds ." seconds ago";
		}
		else if($fullDays == 0 && $fullHours == 0) {
			$str_datetime = $fullMinutes ." minutes ago";
		}
		else if($fullDays == 0 ) {
			$str_datetime = $fullHours ." hours ago";
		}
		else if( $fullDays<30 ) {
			$str_datetime = $fullDays ." days ago";
		}
		else {
			$str_datetime = date("M j, Y H:i A", $var_date);
		}
		
		if ($span_flag == true)
		{
			$str_datetime_span_disp = date("M j, Y H:i A", $var_date);
			$temp = "<span title='$str_datetime_span_disp'>$str_datetime</span>";
			$str_datetime = $temp;
		}
		return $str_datetime;
	}
	else {
		return "--";
	}
}

function fmt_db_date_time($time_var=0)
{
	if ($time_var==0) {
		$time_var = time();
	}
	return date('Y-m-d H:i:s',$time_var);
}



function get_userfullname_by_userid($userids)
{
	$uid=explode(",",$userids);
	
	$string="";

	$userarr=array();

	for($i=0;$i<count($uid);$i++)
	{
		$id=$uid[$i];
		$res=mysql_query("select id,fullname from users where id='$id'")or die(mysql_error());
		$r=mysql_fetch_array($res);
		$userarr[$i]=$r['fullname'];
	}
	$string=implode(",",$userarr);

	return $string;
}


function get_userslist_by_userid($userids)
{
	$uid=explode(",",$userids);
	
	$string="";

	$userarr=array();

	for($i=0;$i<count($uid);$i++)
	{
		$id=$uid[$i];
		$res=mysql_query("select id,fname,lname from users where id='$id'")or die(mysql_error());
		$r=mysql_fetch_array($res);
		$userarr[$i]=ucfirst($r['fname'])." ".$r['lname'];		
	}
	$string=implode(",",$userarr);

	return $string;
}

function limit_words($string, $word_limit)
{
    $words = explode(" ",$string);
    return implode(" ", array_splice($words, 0, $word_limit));
}

function get_user_email($userids)
{
	$uid=explode(",",$userids);

	$userarr=array();

	for($i=0;$i<count($uid);$i++)
	{
		$id=$uid[$i];
		$res=mysql_query("select email from users where id='$id'")or die(mysql_error());
		$r=mysql_fetch_array($res);
		$userarr[$i]=$r['email'];		
	}
	$string=implode(",",$userarr);

	return $string;
}

function get_replyto_users($userids)
{
	$uid=explode(",",$userids);

	for($i=0;$i<count($uid);$i++)
	{
		$id=$uid[$i];
	
		$sql="SELECT * FROM `users` where id='$id' and status='0'";

		//echo $sql;

		$res=mysql_query($sql)or die("Users Err : ".mysql_error());

		$items = array();

		$imgpath=$GLOBALS['imgpath'];

		$j=0;
		while($r=mysql_fetch_array($res))
		{
			$items[$j]['name']=$r['fname']." ".$r['lname'];
			$items[$j]['value']=$r['id'];	
			$j++;

		}
		
	}

	echo json_encode($items);

}

function get_replyto_users2($userids)
{

	$uid=explode(",",$userids);
	
	$string="";

	for($i=0;$i<count($uid);$i++)
	{
		$id=$uid[$i];
	
		$sql="SELECT * FROM `users` where id='$id' and status='0'";

		//echo $sql;

		$res=mysql_query($sql)or die("Users Err : ".mysql_error());

		$items = array();

		$imgpath=$GLOBALS['imgpath'];

		$j=0;
		
		//$string.="[";
		while($r=mysql_fetch_array($res))
		{
			if($j!=0)
				$string.=", ";
				
			$name=$r['fname']." ".$r['lname'];
			$value=$r['id'];

			$string.="{ name:'$name', value:'$value' }";

			$j++;

		}
		//$string.="]";
		
	}

	return $string;

}

?>
