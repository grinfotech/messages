<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>GR InfoTech Messages :: Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <style type="text/css">
    .form-signin
{
    max-width: 330px;
    padding: 15px;
    margin: 0 auto;
}
.form-signin .form-signin-heading, .form-signin .checkbox
{
    margin-bottom: 10px;
}
.form-signin .checkbox
{
    font-weight: normal;
}
.form-signin .form-control
{
    position: relative;
    font-size: 16px;
    height: auto;
    padding: 10px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
.form-signin .form-control:focus
{
    z-index: 2;
}
.form-signin input[type="text"]
{
    margin-bottom: -1px;
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
}
.form-signin input[type="password"]
{
    margin-bottom: 10px;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
}
.account-wall
{
    margin-top: 20px;
    padding: 40px 0px 20px 0px;
    background-color: #f7f7f7;
    -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
}
.login-title
{
    color: #555;
    font-size: 18px;
    font-weight: 400;
    display: block;
}
.profile-img
{
    width: 96px;
    height: 96px;
    margin: 0 auto 10px;
    display: block;
    -moz-border-radius: 50%;
    -webkit-border-radius: 50%;
    border-radius: 50%;
}
.need-help
{
    margin-top: 10px;
}
.new-account
{
    display: block;
    margin-top: 10px;
}   
.errmsg{

	border:1px solid #b30808;
	background:#e09494;
	color:#000;
	padding:8px;

}
 </style>
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript">
    </script>
</head>
<body>
<br>
<?php
include('dbcon.php');
include('genfunctions.php');

$errmsg="";
 
if(isset($_POST['login']))
{

	$un=request_get('un');
	$pwd=request_get('pwd');

	$sql="SELECT * FROM `users` where email='$un' and password='$pwd'";
	$res=mysql_query($sql)or die("Login Err : ".mysql_error());

	$n=mysql_num_rows($res);
	if($n>0)
	{
		$r=mysql_fetch_array($res);
		session_start();
		set_session("LOGIN_UID", $r['id']);	
		set_session("LOGIN_EMAILID", $r['email']);
		set_session("LOGIN_USERID", $r['userid']);				
		set_session("DISPLAY_UNAME", $r['fname']." ".$r['lname']);
		set_session("USER_IMG", $r['imgpath']);
		header("location:inbox.php");
		exit;						
	}else{
		$errmsg="<div class='errmsg'>Invalid Username or Password! </div><br>";
	}
}
?>
<div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <h1 class="text-center login-title">GR InfoTech Messages</h1>
            <div class="account-wall">
                <img class="profile-img" src="images/photo.jpg" alt="">
                <form class="form-signin" method="post">
		<?php
		if(isset($errmsg))
		{
			echo $errmsg;
		}
		?>
                <input type="email" name="un" class="form-control" placeholder="Email" required autofocus>
                <input type="password" name="pwd" class="form-control" placeholder="Password" required>
                <button class="btn btn-lg btn-primary btn-block" type="submit" name="login" >
                    Sign in</button>
                <label class="checkbox pull-left">
                    <input type="checkbox" value="remember-me">
                    Remember me
                </label>
                <a href="#" class="pull-right need-help">Need help? </a><span class="clearfix"></span>
                </form>
            </div>
            <a href="#" class="text-center new-account">Create an account </a>
        </div>
    </div>
</div>	
<script type="text/javascript">
</script>
</body>
</html>
